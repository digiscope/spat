import sys
import re
import subprocess
import shlex



if len (sys.argv) < 4 :
    print ("Usage: ouput_device_name udp_port1 channel1 udp_port2 channel2")
    exit()



ouput_device_name = str(sys.argv[1])
i = 2
streams = {}
while i +1  < len (sys.argv):
    streams[sys.argv[i]]=sys.argv[i+1]
    i+=2

print(streams)

result = subprocess.run(['gst-device-monitor-1.0'], capture_output=True).stdout.decode('utf-8')

#print(result)

p = re.compile(".*?"+ouput_device_name+".*?osxaudiosink device=([0-9]*)",re.DOTALL)
#p = re.compile('\\\[\([0-9]\)\\\]')
m = p.match(result)
if m == None or len(m.groups()) == 0:
    print ("gstreamer does not list "+ouput_device_name+" as an output")
    exit()
output_device_id = m.groups()[0]

print("Device :"+ouput_device_name +" id is " +output_device_id)

processes = []

isLeft=True;
for port in streams.keys():
    if isLeft:
        command_line = ' gst-launch-1.0 udpsrc caps="application/x-rtp,media=(string)audio,clock-rate=(int)48000,encoding-name=(string)OPUS" port='+ port +' ! rtpopusdepay ! opusdec ! audio/x-raw,channels=1 ! audiomixmatrix in-channels=1 out-channels=2 channel-mask=-1 matrix="<<1.0>,<0.0>>>" ! audio/x-raw,channels=2 ! audioconvert ! audioresample ! osxaudiosink device='+output_device_id
        isLeft = False
    else:
        command_line = ' gst-launch-1.0 udpsrc caps="application/x-rtp,media=(string)audio,clock-rate=(int)48000,encoding-name=(string)OPUS" port='+ port +' ! rtpopusdepay ! opusdec ! audio/x-raw,channels=1 ! audiomixmatrix in-channels=1 out-channels=2 channel-mask=-1 matrix="<<0.0>,<1.0>>>" ! audio/x-raw,channels=2 ! audioconvert ! audioresample ! osxaudiosink device='+output_device_id
    print(command_line)
    args = shlex.split(command_line)
    processes.append(subprocess.Popen(args))


print("Press k and ENTER to kill streams and quit")
while True:
    c = sys.stdin.read(1) # reads one byte at a time, similar to getchar()
    if c == 'k':
        break

for p in processes:
    p.kill()

exit()
