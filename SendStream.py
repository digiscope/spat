import sys
import re
import subprocess
import shlex



if len (sys.argv) < 5 :
    print ("Usage: input_device_name ip_address channel1 udp_port1  channel2 udp_port2")
    exit()



print ("Argument List:" + str(sys.argv))


input_device_name = str(sys.argv[1])
ip_address = str(sys.argv[2])
i = 3
streams = {}
while i +1  < len (sys.argv):
    streams[sys.argv[i]]=sys.argv[i+1]
    i+=2

print(streams)


result = subprocess.run(['ffmpeg','-f','avfoundation','-list_devices','true','-i','""'], capture_output=True).stderr.decode('utf-8')

#list_devices_output = os.system('ffmpeg -f avfoundation -list_devices true -i ""')
print(result)

#Looking for [3] Fireface UFX II
p = re.compile(".*?\\[([0-9]*)\\]\s"+input_device_name,re.DOTALL)
#p = re.compile('\\\[\([0-9]\)\\\]')
m = p.match(result)
if m == None or len(m.groups()) == 0:
    print ("ffmepg does not list "+input_device_name+" as an input")
    exit()
input_device_id = m.groups()[0]

print("Device :"+input_device_name +" id is " +input_device_id)


processes = []


for channel in streams.keys():
    command_line = 'ffmpeg -f avfoundation -i ":'+input_device_id+'" -af "pan=mono|c0=c'+str(int(channel)-1)+'" -acodec libopus -application voip -ab 32k -ac 1 -f rtp rtp://'+ip_address+':'+streams[channel]
    print(command_line)
    args = shlex.split(command_line)
    processes.append(subprocess.Popen(args))


print("Press k and ENTER to kill streams and quit")
while True:
    c = sys.stdin.read(1) # reads one byte at a time, similar to getchar()
    if c == 'k':
        break

for p in processes:
    p.kill()

exit()
