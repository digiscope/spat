# Spat


#Start the streams on Wilder:
 - ```python3 ReceiveStream.py BlackHole 5000 1 5001 2``` -> Listen on port 5000 and 5001 and forward those opus mono streams to channel #1and #2 of the BlackHole virtual device.
 - ```python3 SendStream.py Fireface 192.168.0.142 9 5000 10 5001``` -> Get channel #9 and #10 of the Fireface audio device and send two mono opus encoded streams on port 5000 and 5001 of the remote Mac in Wild (192.168.0.142)


#Start the streams on Wild:
 - ```python3 ReceiveStream.py BlackHole 5000 1 5001 2``` -> Listen on port 5000 and 5001 and forward those opus mono streams to channel #1and #2 of the BlackHole virtual device.
 - ```python3 SendStream.py Fireface 192.168.2.4 11 5000 10 5001``` -> Get channel #9 and #10 of the Fireface audio device and send two mono opus encoded streams on port 5000 and 5001 of the remote Mac in Wilder (192.168.2.14)


#Launch Max 
 - Open the Audio status Windows and set the input device to BlackHole and the output device to Fireface
 - Load the 3DSoundDigiscope file and set the IP of the vrpn sources:
   - For Wilder the IP adress for listener is the local Vicon server: 192.168.2.3 and the source IP is Vicon server of Wild: 192.168.0.14
   - For Wild the IP adress for listener is the local Vicon server: 192.168.0.14 and the source IP is Vicon server of Wilder: 192.168.2.3

